﻿//register model
var registermodel = function () {
   // this.username = null;
  //  this.useremail = null;
   // this.userloginid = null;
  //  this.bussinessname = null;
    //this.phonenumber = null;
  //  this.profiletype = null;
   // this.description = null;
}

//forget model
var forgetmodel = function () {
    this.userid = null;
    this.emailid = null;
}

//login model
var loginmodel = function () {
    this.loginid = null;
    this.loginpassword = null;
}

var $login = function () { };
$login = {
    //to load all function 
    init: function () {

        $login.login.init();
        $login.regd.init();
        $login.forget.init();
    },

    //login save 
    login:{
        init: function () {
            $login.login.valid();
            $login.login.action();
        },

        inputNumbers: function ($parent) {
            $parent.find('input.numbers').each(function () {
                $(this).keyup(function () {
                    var _val = $(this).val();
                    $(this).val(_val.replace(/[^0-9\.]/g, ''));
                });
            });
        },
        waitloader: function (text) {
            $("#wait-loader .modal-title").html("");
            $("#wait-loader .modal-title").html(text);
            $("#wait-loader").modal("show");
        },
        closeloader: function () {
            $("#wait-loader").modal("hide");
        },

        initactionloader: function ($el) {
            $el.append('<div id="actionloader"><div class="lds-ring"><div></div><div></div><div></div><div></div></div></div>');
        },

        removeactionloader: function ($el) {
            $('#actionloader').remove();
        },
        //to validate form
        valid: function () {
            $("#loginfrom").validate({
                debug: false,
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                rules: {
                    loginuname: "required",
                    loginupwd: {
                        required: true,
                        minlength: 6
                    },
                },
                messages: {
                    loginuname: "Please enter your login name/email id.",
                    loginupwd: {
                        required: "Please provide a password.",
                        minlength: "Your password must be at least 6 characters long"
                    },
                },
                submitHandler: function (e) {
                    event.preventDefault();
                }
            });

            $("#loginfrom2").validate({
                debug: false,
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                rules: {
                    loginuname: "required",
                    loginupwd: {
                        required: true,
                        minlength: 6
                    },
                },
                messages: {
                    loginuname: "Please enter your login name/email id.",
                    loginupwd: {
                        required: "Please provide a password.",
                        minlength: "Your password must be at least 6 characters long"
                    },
                },
                submitHandler: function (e) {
                    event.preventDefault();
                }
            });
        },

        //to call action on that model
        action: function () {
            $("#loginuname").keyup(function () {
                var val = $("#loginuname").val();
                if (val.indexOf("@") != -1) {
                    $("#loginuname").attr("type", "email");
                }
                else {
                    $("#loginuname").attr("type", "text");
                }
            });
        },

        //to save details
        save: function () {
            var $modal = new loginmodel();
            $modal.loginid = $("#loginuname").val();
            $modal.loginpassword = $("#loginupwd").val();
            var postData = JSON.stringify($modal);
            var formURL = $(this).attr("action");
            $.ajax(
            {
                url: formURL,
                type: "POST",
                data: postData,
                beforeSend: function (xhr) {
                    var $isvalid = $("#loginfrom").valid();
                    if ($isvalid) {
                        alert(postData);
                        $login.login.waitloader("Please wait while message sending.");
                    }
                    return $isvalid;
                },
                success: function (data, textStatus, jqXHR) {
                    //data: return data from server
                    $login.login.closeloader();
                    if (data == true) {
                        swal("Congratulation!", "Message has been sent successfully!", "success");
                        $login.login.reset();
                    }
                    else {
                        swal("Oops!", "Some error has been occurred", "error");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $login.login.closeloader();
                    swal("Oops!", "Some error has been occurred", "error");
                }
            });
            //e.preventDefault(); //STOP default action
            //e.unbind(); //unbind. to stop multiple form submit.
        },

        //to reset form
        reset: function () {
            $('#loginuname').val('');
            $('#loginupwd').val('');
        },
    },

    //register save 
    regd: {
        init: function () {
          //  $layout.inputNumbers($('#registration-modal'));
            $login.login.inputNumbers($('#registration-modal'));
            $login.regd.bussinesstype.init();
            $login.regd.valid();
            $login.regd.action();
        },

        //to call action on that model
        action:function(){
            $("#crprofiletype").select2({
                dropdownParent: $("#registration-modal"),
                minimumResultsForSearch: Infinity,
            });
            $('#crprofiletype').on('select2:select', function (e) {
                $('select[name="crprofiletype"]').valid();
            });
        },

        //to bind bussiness type
        bussinesstype: {
            init: function () {
                //COMMENT THIS WHEN DYNAMIC
                var list = [{ id: 'Community', name: 'Community' },
                    { id: 'Business', name: 'Business' },
                ]
                $login.regd.bussinesstype.set(list);
                //END

                //UNCOMMENT THIS WHEN DYNAMIC
                //    $.when(this.get()).then(function ($data) {
                //         $login.regd.bussinesstype.set($data);
                //    });
            },

            get: function () {
                var dfd = $.Deferred();
                $.ajax({
                    type: "GET",
                    url: "/controller/action",
                    contentType: "application/json; charset=utf-8",
                    datatype: "json",
                    success: function (result) {
                        dfd.resolve($.parseJSON(result));
                        return dfd.promise();
                    },
                    error: function (er) {
                        var _er = er;
                        dfd.fail(_er);
                        return dfd.promise();
                    }
                });
                return dfd.promise();
            },

            set: function ($items) {
                $("#crprofiletype").html('');
                $("#crprofiletype").html('<option></option>');
                $.each($items, function (i, item) {
                    $("#crprofiletype").append('<option value="' + item.id + '">' + item.name + '</option>');
                });
                $('#crprofiletype').change();
            },
        },

        //to validate form
        valid: function () {
            $("#formcreateuser").validate({
                debug: false,
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                rules: {
                    crusername: "required",
                    cruseremail: {
                        required: true,
                        email: true
                    },
                    cruserlogin: "required",
                    crbussinessname: "required",
                    crphnumber: "required",
                    crprofiletype: "required",
                    crdesc:"required"
                },
                messages: {
                    crusername: "Please enter your name.",
                    cruseremail: "Please enter a valid email address.",
                    cruserlogin: "Please enter your login id.",
                    crbussinessname: "Please enter your bussiness name.",
                    crphnumber: "Please enter your phone number.",
                    crprofiletype: "Please select your profile type.",
                    crdesc:"Please enter about us."
                },
                submitHandler: function (e) {
                    event.preventDefault();

                }
            });
        },

        //to load model
        load: function () {
            $login.regd.reset();
            $("#registration-modal").modal('show');
        },

        //to save details
        save: function (flag) {

        	
        	var $modal = new registermodel();
        	if(flag=='false'){        		
	            $modal.userName = $("#crusername").val();;
	            $modal.emailId = $("#cruseremail").val();
	            $modal.loginID = $("#cruserlogin").val();
	            $modal.phone = $("#crphnumber").val();
	            $modal.profileType = $("#crprofiletype").val();
	            $modal.recaptch = $("#captcha").val();
	            debugger
        	}
        	else{
        		$modal.refresh = flag;        	
        	}
        	
        	var postData = JSON.stringify($modal);
    		var formURL = $("#formcreateuser").attr("action");
    		debugger
            $.ajax(
            {
            	url:formURL,
                type: "POST",
                data: postData,
                contentType: "application/json",
                dataType: 'json',
                beforeSend: function (xhr) {
                	
                	if(flag=='false'){   
	                    var $isvalid = $("#formcreateuser").valid();
	                    debugger
	                    if ($isvalid) {
	                      //  alert(postData);
	                      ///  $login.regd.close();
	                      //  $layout.waitloader("Please wait while message sending.");
	                        $login.login.waitloader("Please wait while message sending.");
	                    }
	                    return $isvalid;
                	}
                	else if(flag=='refresh'){
                		return true;
                	}
                },
                success: function (data, textStatus, jqXHR) {
                    //data: return data from server
                   // $layout.closeloader();
                    $login.login.closeloader();
                    $("#captchaImgId").attr("src", 'data:image/jpeg;base64,'+data.responseData);
                   // alert(data.failed);
                    if (data.status == "success") {
                        swal("Success !", "Password hasbeen sent to your mail id!", "success");
                        $login.regd.reset();
                        $login.regd.close();
                    }
                    else if(data.status == "refresh"){
                    	 $("#captchaError").html(data.statusmsg);    
                    }
                    else {
                    	alert(data.statusmsg);
                    	 $("#captchaError").html(data.statusmsg);                    	
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $login.login.closeloader();
                  /*  swal({
                        title: "Oops!",
                        text: "Some error has been occurred.",
                        type: "error"
                    }, function () {
                        $("#registration-modal").modal("hide")
                    });*/
                }
            });
            //e.preventDefault(); //STOP default action
            //e.unbind(); //unbind. to stop multiple form submit.
        },

        //to reset form
        reset: function () {
            $("#crusername").val('');;
            $("#cruseremail").val('');
            $("#cruserlogin").val('');
            $("#crbussinessname").val('');
            $("#crphnumber").val('');
            $("#crprofiletype").val("").trigger('change');
            $("#crdesc").val('');
        },

        //to close model
        close: function () {
            $("#registration-modal").modal("hide")
        },
    },

    //forget save 
    forget: {
        init: function () {
            $login.forget.valid();
        },

        //to load model
        load: function () {
            $login.forget.reset();
            $("#forget-modal").modal('show');
        },
        //to validate form
        valid: function () {
            $("#formfrogetpwd").validate({
                debug: false,
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                success: function (element) {
                    $(element).closest('.form-group').removeClass('has-error');
                },
                rules: {
                    forgetpwdloginid: "required",
                    forgetpwdemail: {
                        required: true,
                        email: true
                    },
                },
                messages: {
                    forgetpwdloginid: "Please enter your user id.",
                    forgetpwdemail: "Please enter a valid email address.",
                },
                submitHandler: function (e) {
                    event.preventDefault();
                }
            });
        },

        //to save details
        save: function () {
            var $modal = new forgetmodel();
            $modal.loginid = $("#forgetpwdloginid").val();
            $modal.email = $("#forgetpwdemail").val();
            $modal.phone = $("#forgetpwdphone").val();
            var postData = JSON.stringify($modal);
          //  var formURL = $(this).attr("action");
            var formURL = "/login/forgot/pw";
            $.ajax(
            {
                url: formURL,
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: postData,
                beforeSend: function (xhr) {
                    var $isvalid = $("#formfrogetpwd").valid();
                    if ($isvalid) {
                        alert(postData);
                        $login.forget.close();
                       // $layout.waitloader("Please wait while message sending.");
                        $login.login.waitloader("Please wait while message sending.");
                    }
                    return $isvalid;
                },
                success: function (data) {
                    //data: return data from server
                	swal( data.statusmsg);
                	$login.login.closeloader();
                   /* if (data == true) {
                        swal("Congratulation!", "Message has been sent successfully!", "success");
                        $login.forget.reset();
                    }
                    else {
                        swal({
                            title: "Oops!",
                            text: "Some error has been occurred.",
                            type: "error"
                        }, function () {
                            $("#forget-modal").modal("hide")
                        });
                    }*/
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $layout.closeloader();
                    swal({
                        title: "Oops!",
                        text: "Some error has been occurred.",
                        type: "error"
                    }, function () {
                        $("#forget-modal").modal("hide")
                    });
                }
            });
            //e.preventDefault(); //STOP default action
            //e.unbind(); //unbind. to stop multiple form submit.
        },

        //to reset form
        reset: function () {
            $("#forgetpwdloginid").val('');
            $("#forgetpwdemail").val('');
        },

        //to close model
        close: function () {
            $("#forget-modal").modal("hide")
        },

    },
}

$(document).ready(function () {
    $login.init();
});