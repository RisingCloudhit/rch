package com.rch.test.rch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RchApplication {

	public static void main(String[] args) {
		SpringApplication.run(RchApplication.class, args);
	}

}
