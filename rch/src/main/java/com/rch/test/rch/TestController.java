package com.rch.test.rch;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TestController {

	
	@GetMapping("/login")
	public String login1(Model model) {

	    model.addAttribute("message", "message");

	    return "login";
	}
}
