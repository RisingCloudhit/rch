package com.rch.test.rch;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface CityRepo  extends JpaRepository<CityMaster, Long>{
	
	public final static String country_list="select json_arrayagg(json_object('id',cpcm.country_id,'name',cpcm.country_name)) from country_phncode_currency_mstr cpcm";
	@Query(nativeQuery=true, value =country_list)
	public String getCountries();

}
