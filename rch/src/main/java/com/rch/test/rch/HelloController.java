package com.rch.test.rch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@Autowired
	private CityRepo cityRepo;
	
	@GetMapping("/")
	public String defaultData() {
		return "Hello and welcome to RCH";
	}
	
	@GetMapping("/home")
	public String homeData() {
		//String countries=cityRepo.getCountries();
		//System.out.println("Countries:: "+countries);
		return "Hello and welcom to home RCH";
	}
}
